# README #

This repository contains scripts for making quick plots of a particular column of data in a standard CHARIS data file.  At the moment, there is just a single script.

### How do I get set up? ###

* Install dependencies:  matplotlib
* Clone this repository
* Run the script with the -h option to see a summary of usage.

Contact:  Bruce Raup (braup@nsidc.org)
