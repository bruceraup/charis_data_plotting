#!/usr/bin/python
""" Program to plot data in the standard CHARIS format

Get help type running \"pydoc <prog_name>\".

"""
#=============================================================================
# 2014-11-17
# Author:  Bruce Raup
# braup@nsidc.org
# 303-492-8814
# National Snow and Ice Data Center,
# University of Colorado, Boulder
# Copyright 2014 Regents of the University of Colorado
#
#
# TODO:
# - Automatically grab axis labels and title from file (DONE)
# - Use pandas to skip plotting of no-data values (get rid of joining line)
#=============================================================================

import os
import argparse
import datetime as dt
import matplotlib as mpl
mpl.use('Agg')  # non-UI backend -- better for apps without display
import matplotlib.pyplot as plt
#from matplotlib.dates import YearLocator, MonthLocator, DateFormatter

version = '0.0.1'
version_string = __file__ + " version " + version


###########################################################
# print_arg_summary
###########################################################
def print_arg_summary(args):
    """Print summary of command-line arguments."""
    print "Arguments object:"
    print args


###########################################################
# setup_argument_parser
###########################################################
def setup_argument_parser():
    """Set up command line options.  -h or --help for help is automatic"""
    desc = """Generate a plot from a standard ASCII CHARIS data file.
    Columns are assumed to be in the format
      "YYYY MM DD [DOY] [TIME] data1 ..."
    See options for including or excluding the DOY and TIME columns."""

    p = argparse.ArgumentParser(description=desc)
    p.add_argument('-c', '--colnum', default="1",
            help='data Column number to plot, starting with 1 after the date columns')
    p.add_argument('-D', '--no_doy',   action='store_true', default=False,
            help="Assume no day-of-year column in the input file")
    p.add_argument('-t', '--time_cols',   action='store_true', default=False,
            help="Assume time columns (HH MM SS) after day or DOY column in the input file")
    p.add_argument('-o', '--outfile', default='charis_data_plot.png',
            help='Output file name for plot')
    p.add_argument('-q', '--quiet',   action='store_true', default=False,
            help="Quiet mode.  Don't print status messages")
    p.add_argument('-V', '--version',   action='version', version=version_string,
            help="Print version and exit")
    p.add_argument('datafile', metavar='FILENAME', type=str, nargs=1,
            help='Name of input CHARIS data file')
    return(p)


###########################################################
# load_input_file
###########################################################
def load_input_file(args):
    fn = args.datafile[0]
    data_obj = {}
    data_obj['dates'] = []
    data_obj['data' ] = []
    data_obj['title'] = os.path.basename(fn)
    data_obj['xlabel']= ''
    data_obj['ylabel']= ''
    data_obj['total_data_lines']= 0.0
    data_obj['num_missing_data']= 0.0

    year_col = 0
    monthcol = 1
    day_col  = 2
    DOY_col  = None
    TIME_col = None     # start of 3 time fields, HH MM SS

    next_col = 3
    if args.no_doy is False:
        DOY_col = next_col
        next_col += 1
    if args.time_cols is True:
        TIME_col = next_col
        next_col += 3

    data_col = next_col + int(args.colnum) - 1

    for line in open(fn, 'r'):
        if line.strip() == '':
            continue
        if line.startswith('##'):
            continue
        if line.startswith('#'):
            field_labels = line[1:].strip().split()
            data_obj['ylabel'] = field_labels[data_col].replace('_', ' ')
            continue
        data_obj['total_data_lines'] += 1.0
        fields = line.strip().split()
        (year, month, day) = fields[0:3]

        datum = float(fields[data_col])
        if datum == -9999.0 or datum == 99999:
            data_obj['num_missing_data'] += 1.0
            continue
        if TIME_col:
            HH = fields[TIME_col]
            MM = fields[TIME_col+1]
            SS = fields[TIME_col+2]
            data_obj['dates'].append(dt.datetime(int(year), int(month), int(day), int(HH), int(MM), int(SS)))
        else:
            data_obj['dates'].append(dt.date(int(year), int(month), int(day)))

        data_obj['data'].append(datum)

    return(data_obj)


###########################################################
# make_plot
###########################################################
def make_plot(args):
    """Make plot of selected data"""
    data_obj = load_input_file(args)
    dates = data_obj['dates']
    data = data_obj['data']
    total_data_lines = data_obj['total_data_lines']
    num_missing_data = data_obj['num_missing_data']
    frac_missing = num_missing_data/total_data_lines

    yearsFmt = mpl.dates.DateFormatter('%Y-%m-%d')
    fig, ax = plt.subplots()
    ax.plot_date(dates, data, '-')

    # format the ticks
    ax.xaxis.set_major_formatter(yearsFmt)
    ax.autoscale_view()

    # format the coords message box
    if args.time_cols:
        ax.fmt_xdata = mpl.dates.DateFormatter('%Y-%m-%d %H:%M')
    else:
        ax.fmt_xdata = mpl.dates.DateFormatter('%Y-%m-%d')
    ax.grid(True)

    # Set title
    ax.set_title(data_obj['title'] + "\n{:.0%} percent missing data".format(frac_missing))
    ax.set_xlabel(data_obj['xlabel'])
    ax.set_ylabel(data_obj['ylabel'])

    fig.autofmt_xdate()
    plt.savefig(args.outfile)
    #plt.show()


###########################################################
# main
###########################################################
def main():
    """Main entry point of the program."""
    p = setup_argument_parser()

    # Parse command-line.
    args = p.parse_args()

    if not args.quiet:
        print_arg_summary(args)

    make_plot(args)

    # End of main


##############################################################################
## Main program
##############################################################################

if __name__ == '__main__':

    # Should put tests in this section if this is primarily a library.  If it
    # is primarily a program, have one simple call to main.

    main()
